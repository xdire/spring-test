package io.xdire.springtest.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;

/**
 * Created by xdire on 9/1/16.
 */
@Configuration
public class i18nConfig {

    @Bean
    public ReloadableResourceBundleMessageSource messageSource() {

        ReloadableResourceBundleMessageSource rrbms = new ReloadableResourceBundleMessageSource();
        // lookup for resources -> i18n -> messages
        rrbms.setBasename("classpath:i18n/messages");
        rrbms.setCacheSeconds(1800);
        return rrbms;

    }

}
