package io.xdire.springtest.web.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Created by xdire on 8/30/16.
 */
@Controller
public class SimpleController {

    @RequestMapping("/index")
    public String indexPage() {
        return "index";
    }

}
