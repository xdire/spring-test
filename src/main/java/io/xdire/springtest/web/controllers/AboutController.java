package io.xdire.springtest.web.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Created by xdire on 9/5/16.
 */
@Controller
public class AboutController {

    @RequestMapping("/about")
    public String about() {
        return "copy/about";
    }

}
