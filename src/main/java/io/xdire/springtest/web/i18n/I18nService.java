package io.xdire.springtest.web.i18n;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Service;

import java.util.Locale;

/**
 * Created by xdire on 9/1/16.
 */
@Service
public class I18nService {
    
    /** Logger entry */
    private static final Logger logger = LoggerFactory.getLogger(I18nService.class);

    @Autowired
    private MessageSource messageSource;

    /**
     *
     * @param messageId
     * @return
     */
    public String getMessage(String messageId) {
        logger.info("i18n text for messageId {}", messageId);
        Locale loc = LocaleContextHolder.getLocale();
        return getMessage(messageId, loc);
    }

    /**
     *
     * @param messageId
     * @param locale
     * @return
     */
    public String getMessage(String messageId, Locale locale) {
        return messageSource.getMessage(messageId, null, locale);
    }

}
