package io.xdire.springtest;

import io.xdire.springtest.web.i18n.I18nService;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * Created by xdire on 9/1/16.
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class SpringTesti18nTests {

    @Autowired
    private I18nService i18srv;

    @Test
    public void testMessageByLocaleService() throws Exception {

        String expected = "Our pretty application";
        String messageId = "index.main.header";
        String actual = i18srv.getMessage(messageId);
        Assert.assertEquals("Not match",expected,actual);

    }

}
